In this project we are going to create a CNN to classify images whether they are cat or dogs.
The dataset is consists of 10000 images and we use 8000 of them as training set and the remaining 2000 images as test set.
We know 8000 images dataset is so small to to train a CNN well. so we use keras image degenerator to crate larger dataset. I consider size of the images (64,64) because I was using CPU and it takes lots of time if you are going to use larger sizes such as (96,96) or (128,128). if you are using GPU feel free to take images size (128,128)
